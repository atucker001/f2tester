import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { F2ProducerComponent } from './f2-producer.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [F2ProducerComponent]
})
export class F2ProducerModule { }
