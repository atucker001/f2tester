import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F2ProducerComponent } from './f2-producer.component';

describe('F2ProducerComponent', () => {
  let component: F2ProducerComponent;
  let fixture: ComponentFixture<F2ProducerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F2ProducerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F2ProducerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
