import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { F2ProducerComponent } from './modules/f2-producer/f2-producer.component';

const appRoutes: Routes = [
  {path: '', component: AppComponent },
  { path: 'app-manifest', component: F2ProducerComponent }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule.withServerTransition({appId: 'universal'})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
